//
//  CoffeeMachine.swift
//  G64DZ6
//
//  Created by mac on 7/24/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    
    var water = 80
    var milk = 120
    var coffee = 30
    
    func espresso() {
        guard water >= 80 else {
            print("Add water")
            return
        }
        if coffee >= 30 {
            water = water - 80
        }
        guard coffee >= 30 else {
            print("Add coffee")
            return
        }
        coffee = coffee - 30
        print("Here is your coffee")
    }
    
    func capuccino() {
        
        guard water >= 80 else {
            print("Add water")
            return
        }
        if coffee >= 30 {
            water = water - 80
        }
        guard coffee >= 30 else {
            print("Add coffee")
            return
        }
        if milk >= 120 {
            coffee = coffee - 30
            print("Here is your coffee")
        }
        
        guard milk >= 120 else {
            print("Add milk")
            return
        }
        if water >= 80 {
            print("Here is your coffee")
            milk = milk - 120
        }
    }
    
    func americano() {
        guard water >= 120 else {
            print("Add water")
            return
        }
        if coffee >= 30 {
            water = water - 120
        }
        
        guard coffee >= 30 else {
            print("Add coffee")
            return
        }
        coffee = coffee - 30
        print("Here is your coffee")
    }
    
    func addWater() {
        water = water + 80
    }
    
    func addCoffee() {
        coffee = coffee + 30
    }
    
    func addMilk() {
        milk = milk + 120
    }
    
}

