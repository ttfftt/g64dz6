//
//  ViewController.swift
//  G64DZ6
//
//  Created by mac on 7/24/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    
    @IBOutlet weak var label: UILabel!
    let coffee = CoffeeMachine()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        coffee
    }
    
    @IBAction func espresso() {
        coffee.espresso()
    }
    
    @IBAction func capuccino() {
        coffee.capuccino()
    }
    
    @IBAction func americano() {
        coffee.americano()
    }
    
    @IBAction func addWater() {
        coffee.addWater()
    }
    
    @IBAction func addCoffee() {
        coffee.addCoffee()
    }
    
    @IBAction func addMilk() {
        coffee.addMilk()
    }
    
    
    
}





